package memory;

import flambe.Component;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.input.MouseCursor;
import flambe.input.PointerEvent;
import flambe.math.Point;
import flambe.System;
import flambe.util.SignalConnection;
import flambe.display.ImageSprite;

class Card extends Component
{
    public function new ()
    {
    }

    private function onPointerDown (event :PointerEvent)
    {
        var sprite = owner.get(Sprite);

        _dragging = true;
        System.mouse.cursor = Button;
    }

    override public function onUpdate (dt :Float)
    {
        if (_dragging) {
            if (System.pointer.isDown()) {
                var sprite = owner.get(Sprite);
                sprite.setRotation(180);
            } else {
                _dragging = false;
                System.mouse.cursor = Default;
            }
        }
    }

    override public function onAdded ()
    {
        _connection = owner.get(Sprite).pointerDown.connect(onPointerDown);
    }

    override public function onRemoved ()
    {
        _connection.dispose();
    }

    public function setSprite(sprite :ImageSprite)
    {
        _sprite = sprite;
    }

    public function getSprite() : ImageSprite
    {
        return _sprite;
    }

    private var _connection :SignalConnection;
    private var _dragging :Bool;
    private var _offset :Point;
    private var _sprite :ImageSprite;
}
