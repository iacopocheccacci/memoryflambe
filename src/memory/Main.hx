package memory;

import flambe.Entity;
import flambe.System;
import flambe.asset.AssetPack;
import flambe.asset.Manifest;
import flambe.display.FillSprite;
import flambe.display.ImageSprite;

class Main
{
    private static function main ()
    {
        // Wind up all platform-specific stuff
        System.init();

        // Load up the compiled pack in the assets directory named "bootstrap"
        var manifest = Manifest.fromAssets("bootstrap");
        var loader = System.loadAssetPack(manifest);
        loader.get(onSuccess);
    }

    private static function onSuccess (pack :AssetPack)
    {
        // Add a filled background color
        System.root.addChild(new Entity()
            .add(new FillSprite(0x303030, System.stage.width, System.stage.height)));

        var deck = new Array<Card>();
        var sprite:ImageSprite = new ImageSprite(pack.getTexture("tentacle"));
        for (ii in 0...29)
        {
            switch (ii) {
                case 0,1:
                    sprite = new ImageSprite(pack.getTexture("03_0001"));
                case 2,3:
                    sprite = new ImageSprite(pack.getTexture("barcaremi01_0001"));
                case 4,5:
                    sprite = new ImageSprite(pack.getTexture("barile01_0001"));
                case 6,7:
                    sprite = new ImageSprite(pack.getTexture("cannone01_0001"));
                case 8,9:
                    sprite = new ImageSprite(pack.getTexture("capitanpolipo01_0001"));
                case 10,11:
                    sprite = new ImageSprite(pack.getTexture("cotton01_0001"));
                case 12,13:
                    sprite = new ImageSprite(pack.getTexture("forziere01_0001"));
                case 14,15:
                    sprite = new ImageSprite(pack.getTexture("ginetto03_0001"));
                case 16,17:
                    sprite = new ImageSprite(pack.getTexture("longjohn02_0001"));
                case 18,19:
                    sprite = new ImageSprite(pack.getTexture("macciu01_0001"));
                case 20,21:
                    sprite = new ImageSprite(pack.getTexture("nave01_0001"));
                case 22,23:
                    sprite = new ImageSprite(pack.getTexture("orlandu01_0001"));
                case 24,25:
                    sprite = new ImageSprite(pack.getTexture("piovra02_0001"));
                case 26,27:
                    sprite = new ImageSprite(pack.getTexture("scimmia01_0001"));
                case 28,29:
                    sprite = new ImageSprite(pack.getTexture("slim01_0001"));
                default:
            }
           
            var card = new Card();
            card.setSprite(sprite);
            deck.push(card);
        }


        var tmp : Card, j : Int, i : Int = deck.length;
        while (i > 0) 
        {
            j = Std.int(Math.random() * i);
            tmp = deck[--i];
            deck[i] = deck[j];
            deck[j] = tmp;
        }

        // Add a bunch of draggable tentacle sprites
        var cardLayer = new Entity();
        var index:Int = 0;
        for (yy in 0...6)
        {
            var yCount:Int = yy + 1;
            for (ii in 0...5) 
            {
                var xCount:Int = ii + 1;
                // var sprite = new ImageSprite(pack.getTexture("tentacle"));
                // sprite.x._ = System.stage.width-((sprite.getNaturalWidth() + 10) * xCount);
                // sprite.y._ = System.stage.height-((sprite.getNaturalHeight() + 10) * yCount);
                // sprite.centerAnchor();


                var cardEntity = new Entity().add(sprite).add(deck[index]);
                cardLayer.addChild(cardEntity);
            }

            index++;
        }
        
        System.root.addChild(cardLayer);

        // Make them zoomable using the mouse wheel or pinching, depending on device support. Note
        // that some environments may have both a mouse and a touch screen
        // if (System.mouse.supported) {
        //     addScrollListener(tentacleLayer);
        // }
        // if (System.touch.supported) {
        //     addPinchListener(tentacleLayer);
        // }
    }
}
